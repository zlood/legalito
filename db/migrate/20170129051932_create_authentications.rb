class CreateAuthentications < ActiveRecord::Migration[5.0]
  def change
    create_table :authentications do |t|
      t.references  :user, foreign_key: true
      t.integer     :attempt

      t.timestamps
    end
  end
end
